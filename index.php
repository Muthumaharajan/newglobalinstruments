
<?php include "main/header.php"; ?>

	<div id="content" class="main-container">
		
		<section class="content-area  pt0 ">
			<div id="main" class="" role="main">
<div id="shapely_home_parallax-2" class="widget shapely_home_parallax">		<section class="cover fullscreen image-bg">
						<div class="parallax-window fullscreen" data-parallax="scroll" data-image-src="<?php echo $img; ?>testimonial-bg-1.jpg" data-ios-fix="true" data-over-scroll-fix="true" data-android-fix="true">
				<div class="align-transform">
					
						<div class="row">

							
							<div class="top-parallax-section">
								<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
									<h1>Welcome To New Global Instruments </h1><div class="mb32"><p>																				Looking for Calibration, Temperature Mapping or Validation Services provider near you?</p>
<p>Why search any more as you have come at a right place for your One-Stop Destination of calibration service requirements that to at a very affordable cost with highest accuracy of tests and reports!!															</p>
</div><a class="btn btn-lg btn-filled" href="calibration-laboratories/index.html">Calibration Services</a><a class="btn btn-lg btn-white" href="front-page/temperaturemappingservices/index.html">Thermal Mapping</a>								</div>
							</div>
							<!--end of row-->
													</div>
					</div>
									</div>
						</section>
		<div class="clearfix"></div>
		</div><div id="shapely_home_parallax-3" class="widget shapely_home_parallax">		<section class="">
								<div class="container">
						
						<div class="row align-children">

															<div class="col-md-7 col-sm-6 text-center mb-xs-24">
									<img class="img-responsive" alt="Temperature Mapping Of Storage Areas" src="<?php echo $img; ?>sensor_distribution.jpg">
								</div>
								
							<div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
								<div class="">
									<h3>Temperature Mapping Of Storage Areas</h3><div class="mb32">
<p class="has-text-align-justify">New Global Instruments based in Qatar and its having branches in Dubai and India has been providing temperature mapping services to its clients in a variety of regulated industries throughout Qatar.</p>
 
<p class="has-text-align-justify"></p>
<p class="has-text-align-justify">Dedicated to providing you with long-term value, our team of temperature mapping of storage area experts have the expertise to ensure you are audit ready and that your&#8230;</p>
</div><a class="btn btn-lg btn-filled" href="front-page/temperaturemappingservices/index.html">Read more</a>								</div>
							</div>
							<!--end of row-->
													</div>
					</div>
							</section>
		<div class="clearfix"></div>
		</div><div id="shapely_home_parallax-5" class="widget shapely_home_parallax">		<section class="small-screen image-bg p0">
						<div class="parallax-window " data-parallax="scroll" data-image-src="<?php echo $img; ?>image_39595.jpg" data-ios-fix="true" data-over-scroll-fix="true" data-android-fix="true">
				<div class="">
					
						<div class="row">

							
							<div class="top-parallax-section">
								<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
									<h1>Buy Testing And Measuring Instruments</h1><div class="mb32"><p>																																												Your trusted partner New Global Instruments Corporation has come up with a new E-Commerce platform for buyers and sellers to showcase and sell multiple products online hassle free with delivery at your Door S																																	</p>
</div><a class="btn btn-lg btn-filled" href="#">Visit Us</a>								</div>
							</div>
							<!--end of row-->
													</div>
					</div>
									</div>
						</section>
		<div class="clearfix"></div>
		</div><div id="shapely_home_contact-2" class="widget shapely_home_contact">
		<section class="contact-section image-bg cover">
			<div data-parallax="scroll" data-image-src="<?php echo $img; ?>Cooden-Innovation-Blog-1024x598-1.jpg" class="parallax-window">
				<div class="container">
					<div class="text-center">
						<h1>Contact us</h1><p class="mb64">																																																																<strong>Reach out to us any time on call or drop an email or meet us at our head office</strong>																																																</p>					</div>
					<div class="row">
						<div class="col-md-4">
							<p class="mb0"><strong>Phone :</strong></p><p class="mb32"><a href="tel:(+974)-74421144">(+974)-74421144</a></p><p class="mb0"><strong>Email :</strong></p><p class="mb32"><a href="mailto:Info@newglobalinstruments.com">Info@newglobalinstruments.com</a></p><p class="mb0"><strong>Address :</strong></p><p class="mb32"><strong>Office No 4, 1st Floor, Building 51,</strong><br />
<br />
<strong>Umm Salal Mohammed, Qatar.</strong></p>						</div>
						<div class="col-md-8">
													</div>
					</div>
				</div>
			</div>
		</section>

		<div class="clearfix"></div>
		</div><div id="shapely_home_clients-2" class="widget shapely_home_clients">					<section>
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h3 class="mb64 mb-xs-40">Our Prime Clients</h3>
						</div>
					</div>
					<!--end of row-->
					<div class="row">
						<div class="logo-carousel">
							<ul class="slides">
																		<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture1.jpg" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture2.png" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture3.png" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture4.jpg" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture5.png" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture6.png" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture7.png" />
											</a>
										</li>
																				<li>
											<a href="#">
												<img alt="Logos" src="<?php echo $img; ?>nlogo/Picture8.jpg" />
											</a>
										</li>
																	</ul>
						</div>
						<!--end of logo slider-->
					</div>
					<!--end of row-->
				</div>
				<!--end of container-->
			</section>
		
		</div>


</div><!-- #main -->
</section><!-- section -->

<?php include "main/footer.php"; ?>