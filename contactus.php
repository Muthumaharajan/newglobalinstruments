<?php include "main/header.php";?>
<div style="background: #333e50 url(<?php echo $img; ?>nlogo/contactbuilging.jpg) repeat-x top center;">
<div id="sectionb_wrapper">
     <div id="sectionb_inner">
          <div id="inner_breadcombmenu">
               <ul>
                    <li><a href="javascript:void(0);"> Home » </a></li>
                    <li><a href="javascript:void(0);" style="color:#01697f;"> Contact Us </a></li>
               </ul>
          </div>
          <h4>CONTACT US</h4>
          <div style="clear:both"></div>
     </div>
</div>
         <div id="contact_wrapper" align="center">
                 <div id="contact_inner">
            
                      <div class="contact_addressbg">
                           <img src="<?php echo $img; ?>nlogo/locationicon_1.png">
                           <h4>Head Office</h4>
                           <p>New Global Instruments<br>
                           Office No 4, 1st Floor, Building 51,<br>
                            Umm Salal Mohammed, Qatar.<br>
                                 General inquiry: Info@newglobalinstruments.com<br>
                                Tel/Fax:(+974)-74421144<br></p>
                           <a class="md-trigger viewmap" data-modal="modal-1"> <img src="<?php echo $img; ?>nlogo/plus.png" style="margin:-4px 5px 0 0;"> VIEW GOOGLE MAP </a>
                           <div class="md-modal md-effect-1" id="modal-1">
                                <div class="md-content">
                                     <a class="md-close close">X</a>
                                </div>
                           </div>
                           <div class="md-overlay"></div>
                      </div>
            
                      <!--contact_addressbg-->
            
            
                      <!--contact_addressbg-->
                      <div style="clear:both"></div>
                 </div><!--contact_inner-->
            </div>
</div>
         <div id="contactform_wrapper" class="post-7 page type-page status-publish hentry">
                 <div id="contactform_inner">
            
                      <h4>Contact Form</h4>
                      <p>Please complete the information below and provide your comments or question. We will forward your request to the correct department or representative to assist you appropriately. You should expect a response within one business day. Thank you for choosing AMIS.</p>
                      <div id="formbg">
            
                           <form method="post" name="form1" id="register-form" novalidate action="" enctype="multipart/form-data">
                                <div class="field_label">
                                     First Name <strong>*</strong>
                                     <input name="firstname" id="firstname" type="text" class="inputf">
                                </div>
            
                                <div class="field_label">
                                     Last Name <strong>*</strong>
                                     <input name="lastname" id="lastname" type="text" class="inputf">
                                </div>
            
                                <div class="field_label">
                                     Email  <strong>*</strong>
                                     <input name="email" id="name" type="text" class="inputf">
                                </div>
            
                                <div class="field_label">
                                     Phone  <strong>*</strong>
                                     <input name="phone" id="phone" type="text" class="inputf">
                                </div>
            
                                <div class="field_label">
                                     Company <strong>*</strong>
                                     <input name="company" id="company" type="text" class="inputf">
                                </div>
            
                                <div class="field_label">
                                     Subject   <strong>*</strong>
                                     <input name="subject" id="subject" type="text" class="inputf">
                                </div>
            
                                <div class="field_label" style="width:98%;">
                                     Question or Comments  <strong>*</strong>
                                     <textarea name="message" id="message" cols="" rows="" class="inputtextarea"></textarea>
                                </div>
                                
                                <div class="field_label">
                                     <div class="g-recaptcha"><div style="width: 304px; height: 78px;"><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div></div>
                                     <div class="divMessage" style="color: #000;"></div>
                                </div>
                                
                                
                                
                                <input type="submit" name="send" value="Submit" class="form_btn">
                           </form>
                      </div><!--formbg-->
                      <div style="clear:both"></div>
                 </div><!--contact_inner-->
            </div>
         


<?php include "main/footer.php";?>