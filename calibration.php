<?php include "main/header.php";?>

<link rel="stylesheet" type="text/css" href="<?php echo $css; ?>calib.css">
          <style>
     .cart_notificationbg{
          font-family: 'PT Sans', sans-serif;
          width:280px;
          height:auto;
          background-color:rgba(178, 22, 20, .9);
          padding:15px 20px;
          position:fixed;
          top:25px;
          line-height:18px;
          right:25px;
          color:#FFF;
          font-size:13px;
          z-index: 999999999;
          -webkit-border-radius: 3px;
          -moz-border-radius: 3px;
          border-radius: 3px;
     }
     .notifi_close{
          color:#FFF;
          text-decoration:none;
          float:right;
     }
     .notifi_close:hover{
          color:#000;
     }
     #msg{
          float:left;
          width:250px;
     }
     @media screen and (max-width: 360px) {
          .cart_notificationbg{
               width:86%; padding:15px 2%; right:5%;}
          #msg{
               width:90%;}
     }
</style>



<!-- php -->          <style>
     .morecontent span {
          display: none;
     }
     .morelink {
          display: block;
     }
     #contentmatter_inner p {
          margin-bottom: 10px !important;}
#contentmatter_inner h4 {
    text-transform: uppercase;
}

.caboxdes {
    height: 168px !important;
}
</style>

<!--INNER-->
<div id="contentmatter_wrapper">
     <div id="contentmatter_inner">
         
         <p style="text-align:justify;">Offering both ISO accredited and 17025 calibration services in Qatar and Serving customers in Aviation, Marine, Manufacturing, Power Transmission, Hospitals, Pharmaceutical, Food Industry, Oil & Gas, Defence & Water treatment we cater to customer requirements from all industries.</p>
         <p style="text-align:justify;">We provide quick onsite calibration services with Qualified Engineers.</p>
         <p style="text-align:justify;">When you calibrate your equipment with us, you can be assured that you’re getting the best calibration services in Qatar. All of instruments are traceable to National or International standards. While we adhere to international standards, if you have a requirement, we can calibrate accordingly as well</p>
         
         
         <p style="text-align:justify;"><strong>Also being the authorised service & distributor of Mitutoyo, KERN, TmepNote, Tulatec, Lascar Electrnonics, WINTERS, we can supply & service for all your product requirements. </strong> <a href="#/services/sub_service/authorized-service-and-calibration-centre" >Click here</a> to know more</p>
         
         
         
         
          <p style="text-align:justify;">We at General Const Lab Calibration (part of General Tech Services) is proud to offer you one-stop solutions for all of your calibration services and new tool requirements.</p>
          
          
          <div style="float:left; width:100%;">
        <div class="calidownload_cer" style="text-align:left;">
            <img src="https://gtae.b-cdn.net/assets/images/eiaac_logo.png" style="width:80px;vertical-align:middle;" > View our accredited calibration scope
            <a download href="images/General_Const._Lab_Calibration_LLC_LB-CAL-004.pdf" target="_blank" class="calidownload">- Download
             <img src="images/down.png" style="margin-top:-5px;"> </a>
             
        </div>
        
       <div class="calidownload_cer2" style="margin-top:20px; float:right;"><img src="https://gtae.b-cdn.net/assets/images/iso9001_logo.png" style="width:130px;vertical-align:middle;" >
            View our ISO9001 Cerificate
            <a download href="images/iso-9001-2015-cert-valid-26-02-2019-until-25-02-2022.pdf" target="_blank" class="calidownload">- Download
             <img src="images/down.png" style="margin-top:-5px;"> </a>
        </div>
        <h1 style="text-align:center;font-size: 29px;      margin-top: 30px;  background: none;">OUR CALIBRATION CAPABILITIES</h1>
 </div> 
 
 
       

                                <div class="calibrationboxbg">
                           <a href="/productview.php?id=1" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/1.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                RCW-800 WIFI Temperature and Humidity Data Logger </div></div>
                           <a class="calimore" href="/productview.php?id=1">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=2" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/2.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                RCW-600 Wifi Temperature data Logger </div></div>
                           <a class="calimore" href="/productview.php?id=2">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=3" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/3.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                Elitech RC-51H PDF USB Temperature and Humidity Data logger </div></div>
                           <a class="calimore" href="/productview.php?id=3">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=4" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/4.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                tempmate.®-M1 </div></div>
                           <a class="calimore" href="/productview.php?id=4">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=5" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/5.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                tempmate.®-S1 V </div></div>
                           <a class="calimore" href="/productview.php?id=5">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=6" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/6.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                Elitech RC-5+ PDF USB Temperature Data logger 32000 </div></div>
                           <a class="calimore" href="/productview.php?id=6">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=7" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/7.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                DR-201 Temperature recorder with printer</div></div>
                           <a class="calimore" href="/productview.php?id=7">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=8" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/8.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                Elitech WT-10 Instant Read Thermometer Self Calibration</div></div>
                           <a class="calimore" href="/productview.php?id=8">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="/productview.php?id=9" class="caliboximage">
                               
                                                                <img src="<?php echo $img; ?>nlogo/9.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <h4 style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;"></h4>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                BT-3 Indoor/ Outdoor Thermometer Hygrometer</div></div>
                           <a class="calimore" href="/productview.php?id=9">Read more</a>
                      </div>
                                            

<?php include "main/footer.php";?>