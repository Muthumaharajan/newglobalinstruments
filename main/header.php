<?php 
$css = "decor/css/";
$js = "decor/js/";
$img = "decor/img/"; 

?>
<!DOCTYPE html>
<html lang="en-GB"
	 >

<!-- Mirrored from newglobalinstruments.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Oct 2022 06:38:03 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">

	<title>New Global Instruments</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- All in One SEO 4.1.8 -->
		<meta name="description" content="New Global Instruments. based in Mumbai with 12 branches across India for Calibration And Temperature Mapping Services in Mumbai, Thane, Bhiwandi, Delhi, Qatar, Qatar, Kolkata, Pune, Chandigarh, Himachal Pradesh, Indore, Maharashtra, Kerala, Gujarat, Ahmedabad, Haryana, Andhra Pradesh." />
		<meta name="robots" content="max-image-preview:large" />
		<link rel="canonical" href="index.php" />

		
		<meta property="og:locale" content="en_GB" />
		<meta property="og:site_name" content="New Global Instruments. - Experience You Can Trust..." />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Calibration And Temperature Mapping Service Company in Mumbai" />
		<meta property="og:description" content="New Global Instruments. based in Mumbai with 12 branches across India for Calibration And Temperature Mapping Services in Mumbai, Thane, Bhiwandi, Delhi, Qatar, Qatar, Kolkata, Pune, Chandigarh, Himachal Pradesh, Indore, Maharashtra, Kerala, Gujarat, Ahmedabad, Haryana, Andhra Pradesh." />
		<meta property="og:url" content="index.php" />
		<meta property="og:image" content="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED.png" />
		<meta property="og:image:secure_url" content="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED.png" />
		<meta property="og:image:width" content="2160" />
		<meta property="og:image:height" content="776" />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:title" content="Calibration And Temperature Mapping Service Company in Mumbai" />
		<meta name="twitter:description" content="New Global Instruments. based in Mumbai with 12 branches across India for Calibration And Temperature Mapping Services in Mumbai, Thane, Bhiwandi, Delhi, Qatar, Qatar, Kolkata, Pune, Chandigarh, Himachal Pradesh, Indore, Maharashtra, Kerala, Gujarat, Ahmedabad, Haryana, Andhra Pradesh." />
		<meta name="twitter:image" content="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED.png" />
		
		<!-- All in One SEO -->

<script>window._wca = window._wca || [];</script>
<link rel='dns-prefetch' href='http://stats.wp.com/' />
<link rel='dns-prefetch' href='http://use.fontawesome.com/' />
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="New Global Instruments. &raquo; Feed" href="index.php" />
<link rel="alternate" type="application/rss+xml" title="New Global Instruments. &raquo; Comments Feed" href="index.php" />
							<script src="http://www.googletagmanager.com/gtag/js?id=UA-89960548-1"  data-cfasync="false" data-wpfc-render="false" type="text/javascript" async></script>
			<script data-cfasync="false" data-wpfc-render="false" type="text/javascript">
				var mi_version = '8.4.0';
				var mi_track_user = true;
				var mi_no_track_reason = '';
				
								var disableStrs = [
															'ga-disable-UA-89960548-1',
									];

				/* Function to detect opted out users */
				function __gtagTrackerIsOptedOut() {
					for ( var index = 0; index < disableStrs.length; index++ ) {
						if ( document.cookie.indexOf( disableStrs[ index ] + '=true' ) > -1 ) {
							return true;
						}
					}

					return false;
				}

				/* Disable tracking if the opt-out cookie exists. */
				if ( __gtagTrackerIsOptedOut() ) {
					for ( var index = 0; index < disableStrs.length; index++ ) {
						window[ disableStrs[ index ] ] = true;
					}
				}

				/* Opt-out function */
				function __gtagTrackerOptout() {
					for ( var index = 0; index < disableStrs.length; index++ ) {
						document.cookie = disableStrs[ index ] + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
						window[ disableStrs[ index ] ] = true;
					}
				}

				if ( 'undefined' === typeof gaOptout ) {
					function gaOptout() {
						__gtagTrackerOptout();
					}
				}
								window.dataLayer = window.dataLayer || [];

				window.MonsterInsightsDualTracker = {
					helpers: {},
					trackers: {},
				};
				if ( mi_track_user ) {
					function __gtagDataLayer() {
						dataLayer.push( arguments );
					}

					function __gtagTracker( type, name, parameters ) {
						if (!parameters) {
							parameters = {};
						}

						if (parameters.send_to) {
							__gtagDataLayer.apply( null, arguments );
							return;
						}

						if ( type === 'event' ) {
							
															parameters.send_to = monsterinsights_frontend.ua;
								__gtagDataLayer( type, name, parameters );
													} else {
							__gtagDataLayer.apply( null, arguments );
						}
					}
					__gtagTracker( 'js', new Date() );
					__gtagTracker( 'set', {
						'developer_id.dZGIzZG' : true,
											} );
															__gtagTracker( 'config', 'UA-89960548-1', {"forceSSL":"true","link_attribution":"true"} );
										window.gtag = __gtagTracker;											(function () {
							var noopfn = function () {
								return null;
							};
							var newtracker = function () {
								return new Tracker();
							};
							var Tracker = function () {
								return null;
							};
							var p = Tracker.prototype;
							p.get = noopfn;
							p.set = noopfn;
							p.send = function (){
								var args = Array.prototype.slice.call(arguments);
								args.unshift( 'send' );
								__gaTracker.apply(null, args);
							};
							var __gaTracker = function () {
								var len = arguments.length;
								if ( len === 0 ) {
									return;
								}
								var f = arguments[len - 1];
								if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
									if ( 'send' === arguments[0] ) {
										var hitConverted, hitObject = false, action;
										if ( 'event' === arguments[1] ) {
											if ( 'undefined' !== typeof arguments[3] ) {
												hitObject = {
													'eventAction': arguments[3],
													'eventCategory': arguments[2],
													'eventLabel': arguments[4],
													'value': arguments[5] ? arguments[5] : 1,
												}
											}
										}
										if ( 'pageview' === arguments[1] ) {
											if ( 'undefined' !== typeof arguments[2] ) {
												hitObject = {
													'eventAction': 'page_view',
													'page_path' : arguments[2],
												}
											}
										}
										if ( typeof arguments[2] === 'object' ) {
											hitObject = arguments[2];
										}
										if ( typeof arguments[5] === 'object' ) {
											Object.assign( hitObject, arguments[5] );
										}
										if ( 'undefined' !== typeof arguments[1].hitType ) {
											hitObject = arguments[1];
											if ( 'pageview' === hitObject.hitType ) {
												hitObject.eventAction = 'page_view';
											}
										}
										if ( hitObject ) {
											action = 'timing' === arguments[1].hitType ? 'timing_complete' : hitObject.eventAction;
											hitConverted = mapArgs( hitObject );
											__gtagTracker( 'event', action, hitConverted );
										}
									}
									return;
								}

								function mapArgs( args ) {
									var arg, hit = {};
									var gaMap = {
										'eventCategory': 'event_category',
										'eventAction': 'event_action',
										'eventLabel': 'event_label',
										'eventValue': 'event_value',
										'nonInteraction': 'non_interaction',
										'timingCategory': 'event_category',
										'timingVar': 'name',
										'timingValue': 'value',
										'timingLabel': 'event_label',
										'page' : 'page_path',
										'location' : 'page_location',
										'title' : 'page_title',
									};
									for ( arg in args ) {
																				if ( ! ( ! args.hasOwnProperty(arg) || ! gaMap.hasOwnProperty(arg) ) ) {
											hit[gaMap[arg]] = args[arg];
										} else {
											hit[arg] = args[arg];
										}
									}
									return hit;
								}

								try {
									f.hitCallback();
								} catch ( ex ) {
								}
							};
							__gaTracker.create = newtracker;
							__gaTracker.getByName = newtracker;
							__gaTracker.getAll = function () {
								return [];
							};
							__gaTracker.remove = noopfn;
							__gaTracker.loaded = true;
							window['__gaTracker'] = __gaTracker;
						})();
									} else {
										console.log( "" );
					( function () {
							function __gtagTracker() {
								return null;
							}
							window['__gtagTracker'] = __gtagTracker;
							window['gtag'] = __gtagTracker;
					} )();
									}
			</script>
				
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='nextcodeslider-animate-css-css'  href='<?php echo $css; ?>animate.minc64e.css?ver=1.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='nextcodeslider-css'  href='<?php echo $css; ?>style.minc64e.css?ver=1.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='nextcodeslider-swiper-css'  href='<?php echo $css; ?>swiper.minc64e.css?ver=1.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='nextcodeslider-fontawesome-css'  href='<?php echo $css; ?>allc64e.css?ver=1.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='cuar.frontend-css'  href='<?php echo $css; ?>styles.min3752.css?ver=8.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='<?php echo $css; ?>style.min.css' type='text/css' media='all' />
<style id='wp-block-library-inline-css' type='text/css'>
.has-text-align-justify{text-align:justify;}
</style>
<link rel='stylesheet' id='wc-block-style-css'  href='<?php echo $css; ?>style.css' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='<?php echo $css; ?>woocommerce-layout.css' type='text/css' media='all' />
<style id='woocommerce-layout-inline-css' type='text/css'>

	.infinite-scroll .woocommerce-pagination {
		display: none;
	}
</style>
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='<?php echo $css; ?>woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='<?php echo $css; ?>woocommerce.css' type='text/css' media='all' />
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='wp-review-slider-pro-public_combine-css'  href='<?php echo $css; ?>wprev-public_combine9122.css?ver=6.6' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='<?php echo $css; ?>bootstrap.mine6b3.css?ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='<?php echo $css; ?>font-awesome.mine6b3.css?ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='shapely-fonts-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C300%2C400%2C500%2C600%2C700&amp;ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-css'  href='<?php echo $css; ?>flexslidere6b3.css?ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='shapely-style-css'  href='<?php echo $css; ?>stylee6b3.css?ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='owl.carousel-css'  href='<?php echo $css; ?>owl.carousel.mine6b3.css?ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='owl.carousel.theme-css'  href='<?php echo $css; ?>owl.theme.defaulte6b3.css?ver=5.4.11' type='text/css' media='all' />
<link rel='stylesheet' id='jetpack_css-css'  href='<?php echo $css; ?>jetpack.css' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo $js; ?>jquery.js'></script>
<script type='text/javascript' src='<?php echo $js; ?>jquery-migrate.min.js'></script>
<script type='text/javascript' src='<?php echo $js; ?>three.minc64e.js?ver=1.1.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>swiper.minc64e.js?ver=1.1.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>Tween.minc64e.js?ver=1.1.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>webfontloaderc64e.js?ver=1.1.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>embedc64e.js?ver=1.1.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>nextcodeSliderc64e.js?ver=1.1.1'></script>

<script type='text/javascript' src='<?php echo $js; ?>frontend-gtag.min2678.js?ver=8.4.0'></script>
<script async type='text/javascript' src='<?php echo $js; ?>s-202240.js'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.4.11" />
<meta name="generator" content="WooCommerce 4.0.4" />
<link rel='shortlink' href='index.php' />
<link rel='dns-prefetch' href='http://c0.wp.com/'/>
<style type='text/css'>img#wpstats{display:none}</style><style type="text/css"></style>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<link rel="icon" href="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-1-32x32.png" sizes="32x32" />
<link rel="icon" href="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-1-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon" href="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-1-180x180.png" />
<meta name="msapplication-TileImage" content="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-1-270x270.png" />
</head>

<body class="home page-template page-template-page-templates page-template-template-home page-template-page-templatestemplate-home-php page page-id-37 wp-custom-logo theme-shapely woocommerce-no-js customer-area-active has-sidebar-right">
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

	<header id="masthead" class="site-header" role="banner">
		<div class="nav-container">
			<nav  id="site-navigation" class="main-navigation" role="navigation">
				<div class="container nav-bar">
				<div class="flex-row"><small style="float:left;margin-left: 500px;color:black;">24/7 Call/whatsapp: +97431671460</small><small style="float:right;color:black;">Email: info@newglobalinstruments.com</small></div>
					<div class="flex-row">
						<div class="module left site-title-container">
							<a href="index.php" style="margin-top: -600px;" class="" rel="home" itemprop="url"><img width="180" height="45" src="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-300x108.png" class="custom-logo logo" alt="New Global Instruments." itemprop="logo" srcset="<?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-300x108.png 300w, <?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-600x216.png 600w, <?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-1024x368.png 1024w, <?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-768x276.png 768w, <?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-1536x552.png 1536w, <?php echo $img; ?>cropped-VEGA-CALVAL-FINAL-CROPED-2048x736.png 2048w" sizes="(max-width: 125px) 100vw, 125px" /></a>						</div>
						<button class="module widget-handle mobile-toggle right visible-sm visible-xs">
							<i class="fa fa-bars"></i>
						</button>
						<div class="module-group right">
							<div class="module left">
								<div class="collapse navbar-collapse navbar-ex1-collapse"><ul id="menu" class="menu"><li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-15 active"><a title="Home" href="index.php">Home</a></li>
<li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16"><a title="About Us" href="aboutus.php">About Us</a></li>
<li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16"><a title="Products" href="calibration.php">Products</a></li>
<li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16"><a title="Temperature Mapping" href="thermalmap.php">Temperature Mapping</a></li>
<li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16"><a title="Calibration Services" href="calibrationservice.php">Calibration Services</a></li>
<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a title="Blog" href="blog.php">News</a></li>
<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18 "><a title="Contact Us" href="contactus.php">Contact</a><span class="dropdown-toggle shapely-dropdown" data-toggle="dropdown"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
</li>
</ul></div>							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</header>
