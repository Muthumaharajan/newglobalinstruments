<?php include "main/header.php";?>

<link rel='stylesheet' id='cfb-flexboxgrid-style-css'  href='<?php echo $css; ?>flipboxes-flexboxgrid.min4735.css?ver=1.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='cfb-styles-css'  href='<?php echo $css; ?>flexslidere6b3.css?ver=1.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='cfb-styles-css'  href='<?php echo $css; ?>flipboxes-styles.min4735.css?ver=1.7.1' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo $js; ?>flipboxes-custom.min4735.js?ver=1.7.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>jquery.flip.min4735.js?ver=1.7.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>jquery-imagesloader.min4735.js?ver=1.7.1'></script>

<div id="content" class="main-container">
					<div class="header-callout">
									</div>
		
		<section class="content-area  pt0 ">
			<div id="main" class="container" role="main">

	<div class="row">
		<div id="primary" class="col-md-12 mb-xs-24 full-width">
			<article id="post-441" class="post-content post-grid-wide post-441 page type-page status-publish hentry">
	<header class="entry-header nolist">
			</header><!-- .entry-header -->
	<div class="entry-content">
					<h2 class="post-title entry-title">
				<a href="index.html">Pressure Calibration Services</a>
			</h2>
		
		<div class="entry-meta">
			
		<ul class="post-meta">
							<li><span>by <a href="index.php" title="New Global Calibrations LLP.">New Global Calibrations LLP.</a></span></li>
				</ul>
					<!-- post-meta -->
		</div>

					<div class="shapely-content dropcaps-content">
				
<div class="wp-block-cover has-background-dim has-parallax" style="background-image:url(<?php echo $img; ?>empty-white-lab-with-concrete-floor-and-fluorescent-lights-above.jpg)"><div class="wp-block-cover__inner-container">
<p class="has-text-align-center has-large-font-size"><div id="flipbox-widget-455" class="cfb_wrapper layout-4 flex-row" data-flipboxid="flipbox-widget-455"><div class="flex-col-md-6 cfb-box-1 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="x" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>PG.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:52px;border-color:#28aeb7;color:#28aeb7">
                        <i class="fa fa-arrow-circle-up"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#28aeb7">Pressure Gauge Calibration</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#28aeb7">
                <p>Pressure Gauges are calibrated over the range from 0 to 750 bar (calibration results provided in all units as per the instrument). Test Bench for In House and Hand Pump for On-Site Calibration is used for the calibration activity.</p></div>
            </div>
          </div><div class="flex-col-md-6 cfb-box-2 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="x" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>Pressure-gauge-calibration.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:52px;border-color:#77455f;color:#77455f">
                        <i class="fa fa-arrow-circle-up"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#77455f">Vacuum Gauge Calibration</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#77455f">
                <p>Vacuum Gauges are calibrated over the range from 0 to -760 mmHg (calibration results provided in all units as per the instrument). Test Bench for In House and Hand Pump for On-Site Calibration is used for the calibration activity.</p></div>
            </div>
          </div><div class="flex-col-md-6 cfb-box-3 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="x" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>Pressure-Sensor-e1589207424407.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:52px;border-color:#3e4ac9;color:#3e4ac9">
                        <i class="fa fa-arrow-circle-up"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#3e4ac9">Pressure Sensor Calibration</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#3e4ac9">
                <p>Pressure Sensors are calibrated over the range as per instrument (calibration results provided in all units as per the instrument).</p></div>
            </div>
          </div><div class="flex-col-md-6 cfb-box-4 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="x" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>Pressure-Switch.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:52px;border-color:#e2c102;color:#e2c102">
                        <i class="fa fa-arrow-circle-up"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#e2c102">Pressure Switch Calibration</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#e2c102">
                <p>Pressure Switches are calibrated over the range as per instrument (calibration results provided in all units as per the instrument).</p></div>
            </div>
          </div></div></p>
</div></div>
			</div>
			</div><!-- .entry-content -->

	</article>
		</div><!-- #primary -->
	</div>

</div><!-- #main -->
</section><!-- section -->

<?php include "main/footer.php";?>