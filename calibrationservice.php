<?php include "main/header.php";?>

<link rel="stylesheet" type="text/css" href="<?php echo $css; ?>calib.css">
          <style>
     .cart_notificationbg{
          font-family: 'PT Sans', sans-serif;
          width:280px;
          height:auto;
          background-color:rgba(178, 22, 20, .9);
          padding:15px 20px;
          position:fixed;
          top:25px;
          line-height:18px;
          right:25px;
          color:#FFF;
          font-size:13px;
          z-index: 999999999;
          -webkit-border-radius: 3px;
          -moz-border-radius: 3px;
          border-radius: 3px;
     }
     .notifi_close{
          color:#FFF;
          text-decoration:none;
          float:right;
     }
     .notifi_close:hover{
          color:#000;
     }
     #msg{
          float:left;
          width:250px;
     }
     @media screen and (max-width: 360px) {
          .cart_notificationbg{
               width:86%; padding:15px 2%; right:5%;}
          #msg{
               width:90%;}
     }
</style>



<!-- php -->          <style>
     .morecontent span {
          display: none;
     }
     .morelink {
          display: block;
     }
     #contentmatter_inner p {
          margin-bottom: 10px !important;}
#contentmatter_inner h4 {
    text-transform: uppercase;
}

.caboxdes {
    height: 168px !important;
}
</style>

<!--INNER-->

<div id="sectionb_wrapper">
     <div id="sectionb_inner">
          <div id="inner_breadcombmenu">
               <ul>
                    <li><a href="#"> Home &raquo; </a></li>
                    <li><a href="#" style="color:#d92523;"> Services </a></li>
               </ul>
          </div><!--inner_breadcombmenu-->
          <h1>SERVICES</h1>
          <div style="clear:both"></div>
     </div><!--sectionb_inner-->
</div><!--sectionb_wrapper-->

<!--INNER-->
<div id="contentmatter_wrapper">
     <div id="contentmatter_inner">
        
         

                                <div class="calibrationboxbg">
                           <a href="#services/sub_service/dimensional-inspection-and-cmm-inspections-services" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>8630951renishaw_ph20.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;"> Dimensional Inspection and CMM Inspections Services</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    &nbsp;

&nbsp; The Dimensional Inspection Lab of General Construction&nbsp;Lab&nbsp;is an EIAC Accredited ISO-17025 Certified Calibration Lab. By utilizing state of the art Mitutoyo measuring equipment,&nbsp;other reference instruments, and supporting software of unsurpassed quality, we can provide virtually unlimited Dimensional Inspection Capabilities and CMM Inspections Services.

Our Dimensional Inspection services are complete with a full Metrology Lab and High-Accuracy Coordinate Measuring Machines (CMM), Video Measuring Capabilities for extra small items like printed circuit boards, a 12&Prime; Optical Comparator as well as a full range of precision dimensional measurement instruments.

&nbsp;

We are able to provide precision measurements in a timely and efficient manner with virtually unlimited Metrology Services. Using Mitutoyo 9106, we have the capacity to inspect&nbsp;parts up to 900 x 1000 x 690 mm.

Carrying out comparison CMM inspection to the drawings provided for samples, every test carried out is backed with a traceable certificate for reference.

&nbsp;

In addition, being the authorized distributors for Mitutoyo, we can provide a quote for new CMM&#39;s as well. Contact us to know more.&nbsp;                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/dimensional-inspection-and-cmm-inspections-services">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/valve-testing-servcies" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>66992696114915services_2.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">Valve Testing Servcies</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    Malfunctioning valves can be risky and getting the right replacements can take months. Faulty valves lead to potential downtime, inefficient plant operations and in worst case; it even poses a safety risk.

Offering both on-site and in-house calibration services, we have a solution for every requirement. Our service team consists of highly qualified and experienced personnel possessing sound knowledge and hands on experience about all types of valve from various manufacturers.

At New Global Instruments, jobs are carried out according to procedures conforming to the requirements of BS EN ISO 4126-1:2013, API-598 and API 576 standards.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/valve-testing-servcies">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/repairs" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>97097214774214services_4.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">Repairs</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    Equipment&#39;s usually have a life cycle and sometimes can fail unexpectedly. Having to purchase a replacement can be a costly investment and we understand this hassle.

At New Global Instruments, using our extensive knowledge of calibration, servicing and repair - we are capable of addressing your needs. All repairs carried out are performed by highly technical and capable engineers and is thoroughly tested before dispatch.

Some of the items we repair include Oven, Furnace, Compression machine, Multimeter, Weigh balance and Dimensional instrument.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/repairs">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/authorized-service-and-calibration-centre" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>5807650Authorised_Service_Center.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">Authorized service and calibration centre</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    New Global Instruments Services is the Authorised Service and Calibration center for Mitutoyo, Gagemaker, WIKA DH Budenberg, Snap On Torque tools &amp; Mark 10.&nbsp; We offer calibration services in UAE &amp; Saudi Arabia.

Capable of repairing and servicing pressure gauges, dead weight testers and more, we are&nbsp;certified to issue 17025 calibration certificates for dead weight testers.

We also offer calibration services for Gagemaker products. We are the only calibration laboratory equipped to calibrate Mictrac&#39;s using the appropriate instruments.

To know more about our other calibration page, please click here.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/authorized-service-and-calibration-centre">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/gagemaker-approved-mictrac-calibration" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>36634109921337Renshaw_XL_80.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">GAGEMAKER approved MICTRAC calibration</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    The&nbsp;Gagemaker MIC TRAC&trade; 3000 Series Gage Setting and Part Measurement System&nbsp;sets or zeros most gages with the standard resolution of .00005&Prime;, accuracies ranging from &plusmn;.0001&Prime; to &plusmn;.0004&Prime; (traceable to the NIST), and the digital read out is CE approved. The cost effective Force-Lok feature improves repeatability from operator to operator and is well suited for a variety of environments including the shop floor.

The Gagemaker In-House MIC TRAC&trade;&nbsp;4000 Series Precision Gage Calibration System enables you to inspect parts, calibrate and track a wide variety of gage styles, including ring and plugs. This innovative high precision metrology system with 1.00&Prime; travel guarantees extremely high accuracies to &plusmn;.00005 and a resolution of .00001&Prime; (traceable to the NIST appropriate national standard). This system also enables you to track and maintain gage history as well as produces calibration certificates.

But how do you calibrate Mic trac&#39;s? Earlier you&#39;d have to send it back to Gagemaker for re-calibration. However, now New Global Instruments can calibrate Mic trac&#39;s in UAE. Using reference instruments with higher accuracy, we are now equipped to have the Mic Trac&#39;s calibrated for you in UAE. You can have the instrument calibrated and returned in days and not weeks anymore !&nbsp;                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/gagemaker-approved-mictrac-calibration">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/vfd-repair" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>55919114900375VFD_repair.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">VFD Repair</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    New Global Instruments Services is capable to&nbsp;service, repair and refurbish AC, DC and VFD Drive&nbsp;and other electrical control systems in UAE.

We have&nbsp;the tools, ability, and experience to accurately diagnose problems with drives, controls, and circuit breakers. Our resources include manuals, guides, and schematics that give us the advantage when it comes to troubleshooting and repair. Our reputable experts have extensive knowledge of many OEM controls and circuit breakers, including Control techniques (We are an authorized Control Techniques Distributor)&nbsp; &nbsp;Our team of knowledgeable technicians have&nbsp;the skills to repair, refurbish, and retrofit large industrial circuit breakers

Some of the brands that we repair&nbsp;are ABB AC Drive, Siemens Drive , Allen Bradley Servo Drive, Cutler- Hammer, ALTIVAR AEG DC Drive , Allen Bradley Inverter, Emerson, Eurotherm, Invertek,&nbsp;Hitachi, Mitsubishi, Delta Drive and Kawasaki Drive.&nbsp;

We offer site inspection services as well                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/vfd-repair">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/pat-portable-appliance-testing-services" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>5454317pat.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">PAT (Portable Appliance Testing) Services</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    New Global Instruments Services carries out comprehensive PAT testing for electrical appliances using the best PAT tester in the market. Using an accredited PAT tester, we ensure that our reference instrument is properly functioning before we test your electrical equipment.

&nbsp;

&nbsp;                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/pat-portable-appliance-testing-services">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/cmm-calibration-services" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>7059307CMM.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">CMM Calibration Services</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    We provide traceable&nbsp;CMM Calibration&nbsp;services.&nbsp;Our skilled, detailed-oriented CMM Service Engineers apply those approved methods and processes to guarantee that your CMM is calibrated to&nbsp;ISO 10360&nbsp;standards. During the process, we utilize an extremely precise step gauge artifact with NIST traceability,&nbsp;ensuring&nbsp;that your CMM is performing optimally and providing accurate and repeatable measurements.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/cmm-calibration-services">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/power-quality-analysis-monitoring-solutions" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>7856990Power_quality_analysis-min.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">Power Quality Analysis & Monitoring Solutions</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    Power Quality Audit is used to determine how a building or installation&#39;s power can be improved. This is generally triggered by evident problems like machinery malfunctions or power outages. A more thorough audit will reveal hidden issues, and the changes taken as a result will have a substantial impact on a company&#39;s energy consumption and costs over time.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/power-quality-analysis-monitoring-solutions">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/vfd-and-starter-panel-customization" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>5977646ct_vfd.jpg" class="serviceimg_2"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">VFD and Starter Panel Customization</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    Customers can request customized VFD and starter panels from New Global Instruments based on their specifications and standards. We understand our clients&#39; requirements and will design an application that meets those requirements. We can also modify the design in response to customer feedback. We are capable of producing and delivering a high-quality product to the customer within the time frame specified. As Control Techniques VFD distributors, we provide clients with the best value at a low cost.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/vfd-and-starter-panel-customization">Read more</a>
                      </div>
                                            <div class="calibrationboxbg">
                           <a href="#services/sub_service/rental-services-calibrators-chart-recorders-hart-communicator-hydro-test-pumps" class="caliboximage">
                                                                                                  <img src="<?php echo $img; ?>9901162Rental_services.jpg" class="serviceimg_1"  />
                                                           </a>
                           <div class="caboxdes">
                                <strong style="height: 50px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;color:#d72422;">Rental Services- Calibrators, Chart Recorders, HART Communicator, Hydro test pumps</strong>
                                <div align="justify" style="display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical; overflow: hidden; width: 100%;" class="more">
                                    New Global Instruments Services provides a quick and cost-effective way to obtain the equipment you require. Our dependable rental services deliver pre-calibrated, ready-to-use equipment to you in a flexible manner that meets your needs.                                                                     </div>
                           </div>
                           <a class="calimore" href="#services/sub_service/rental-services-calibrators-chart-recorders-hart-communicator-hydro-test-pumps">Read more</a>
                      </div>
                                <div style="clear:both"></div>
     </div><!--contentmatter_inner-->
</div>
<?php include "main/footer.php";?>