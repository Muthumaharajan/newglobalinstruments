<?php include "main/header.php";?>
<div id="content" class="main-container">
					<div class="header-callout">
									</div>
		
		<section class="content-area  pt0 ">
			<div id="main" class="container" role="main">
	<div class="row">
				<div id="primary" class="col-md-8 mb-xs-24 no-sidebar">
																
<article id="post-9" class="post-9 page type-page status-publish hentry">
	<header class="entry-header">
				<h1 class="entry-title">Blog</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		
<h1><strong>WHAT IS CALIBRATION?</strong></h1>



<div class="wp-block-image"><figure class="alignleft"><a href="<?php echo $img; ?>Capture.jpg"><img src="<?php echo $img; ?>Capture.jpg" alt="" class="wp-image-51"/></a></figure></div>



<div class="wp-block-image"><figure class="aligncenter"><img src="<?php echo $img; ?>scale-154924_1280-262x300.png" alt="Analog Weighing Scale" class="wp-image-63"/><figcaption>Analog Weighing Scale</figcaption></figure></div>



<p>Before we define&nbsp; what does calibration mean is, I want you to understand below&nbsp; scenario:</p>



<p>When you go to the market and buy some meat, you bought 1 kilo and paid it with your hard-earned money but when you checked it at home, it is only ¾ of a kilo, what would you feel?</p>



<p>You just gassed-up good for 2 days as your regular routine but only a day had passed and your meter indicates near empty – would this not make you mad?</p>



<div class="wp-block-image"><figure class="aligncenter"><img src="<?php echo $img; ?>fuel-40193_640-300x231.png" alt="fuel-40193_640" class="wp-image-61"/></figure></div>



<div class="wp-block-image"><figure class="alignleft"><a href="<?php echo $img; ?>oven.jpg"><img src="<?php echo $img; ?>oven-300x200.jpg" alt="" class="wp-image-64"/></a></figure></div>



<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </p>



<p></p>



<p></p>



<p>You are baking a&nbsp; cake and the instruction tells you to set the temperature to 50 deg Celsius for 20 minutes,&nbsp;you followed the steps and specification but your cake turned into charcoal – will you not be upset?</p>



<p>Now, what do you feel if the above scenario happens to you? Of course, you may get angry, dismayed or worse complain to the services or product that you received and paid for, all because of the effect of the un-calibrated Instruments.</p>



<p>This is why calibration is important in our daily life not just inside a laboratory or within the manufacturing aspect. There is the involvement of quality, safety, and reliability.</p>
	</div><!-- .entry-content -->
		<footer class="entry-footer">
			</footer><!-- .entry-footer -->
</article><!-- #post-## -->
		</div><!-- #primary -->
			</div>

</div><!-- #main -->
</section><!-- section -->
<?php include "main/footer.php";?>