<?php include "main/header.php";?>

<link rel='stylesheet' id='cfb-flexboxgrid-style-css'  href='<?php echo $css; ?>flipboxes-flexboxgrid.min4735.css?ver=1.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='cfb-styles-css'  href='<?php echo $css; ?>flexslidere6b3.css?ver=1.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='cfb-styles-css'  href='<?php echo $css; ?>flipboxes-styles.min4735.css?ver=1.7.1' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo $js; ?>flipboxes-custom.min4735.js?ver=1.7.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>jquery.flip.min4735.js?ver=1.7.1'></script>
<script type='text/javascript' src='<?php echo $js; ?>jquery-imagesloader.min4735.js?ver=1.7.1'></script>

<div id="content" class="main-container">
					<div class="header-callout">
									</div>
		
		<section class="content-area  pt0 ">
			<div id="main" class="container" role="main">

	<div class="row">
		<div id="primary" class="col-md-12 mb-xs-24 full-width">
			<article id="post-431" class="post-content post-grid-wide post-431 page type-page status-publish hentry">
	<header class="entry-header nolist">
			</header><!-- .entry-header -->
	<div class="entry-content">
					<h2 class="post-title entry-title">
				<a href="index.html">Thermal Calibration Services</a>
			</h2>
		
		<div class="entry-meta">
			
		<ul class="post-meta">
							<li><span>by <a href="index.php" title="New Global Instruments.">New Global Instruments.</a></span></li>
				</ul>
					<!-- post-meta -->
		</div>

					<div class="shapely-content dropcaps-content">
				
<div class="wp-block-cover has-background-dim has-parallax" style="background-image:url(<?php echo $img; ?>tolkim_egitim_laboratuvari_3.jpg)"><div class="wp-block-cover__inner-container">
<p class="has-text-align-center has-large-font-size"><div id="flipbox-widget-430" class="cfb_wrapper layout-4 flex-row" data-flipboxid="flipbox-widget-430"><div class="flex-col-md-4 cfb-box-1 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="y" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>VegaCalibrationSensor.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:200px;border-color:#273ace;color:#273ace">
                        <i class="fa fa-adjust"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#273ace">Calibration Of Temperature Sensors</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#273ace">
                <p>Temperature Sensors such as Thermocouples, RTD's, Simplex, Duplex, etc. calibrated over the range from -80°C Upto 1150°C with highly accurate test chambers such as Low Temperature Bath, Water Bath and Dry Block as a source.</p></div>
            </div>
          </div><div class="flex-col-md-4 cfb-box-2 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="y" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>RC-4-4HC-5-5-GSP-6-Digital-USB-Temperature-Humidity-Data-Logger-Built-in-NTC.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:200px;border-color:#efc610;color:#efc610">
                        <i class="fa fa-adjust"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#efc610">Calibration Of Temperature Data Loggers</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#efc610">
                <p>Wireless Temperature Data Loggers of all types calibrated over the range from -20°C Upto 50°C in a very stable and accurate Deep Freezer Chamber.</p></div>
            </div>
          </div><div class="flex-col-md-4 cfb-box-3 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="y" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>wifi.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:200px;border-color:#09ba44;color:#09ba44">
                        <i class="fa fa-adjust"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#09ba44">Calibration Of Temperature And Humidity Data Loggers</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#09ba44">
                <p>Wireless Temperature &amp; Humidity Data Loggers of all types calibrated over the range from -20°C Upto 50°C and 10% to 95% RH in a very stable and accurate Stability Chamber.</p></div>
            </div>
          </div><div class="flex-col-md-4 cfb-box-4 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="y" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>Oven.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:200px;border-color:#ad06a7;color:#ad06a7">
                        <i class="fa fa-adjust"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#ad06a7">Calibration Of Ovens</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#ad06a7">
                <p>Ovens of all types calibrated over the range from 0°C Upto 1100°C with the help of highly accurate test sensors (K Type, S Type) and 6.5 Digit Multimeter to provide highest resolution and unmatched accuracy.</p></div>
            </div>
          </div><div class="flex-col-md-4 cfb-box-5 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="y" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>furnace-calibration.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:200px;border-color:#dd700b;color:#dd700b">
                        <i class="fa fa-adjust"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#dd700b">Calibration Of Furnaces</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#dd700b">
                <p>Furnaces of all types calibrated over the range from 0°C Upto 1100°C with the help of highly accurate test sensors (K Type, S Type) and 6.5 Digit Multimeter to provide highest resolution and unmatched accuracy.</p></div>
            </div>
          </div><div class="flex-col-md-4 cfb-box-6 cfb-box-wrapper">
            <div class="flipbox-container cfb-layout-4 cfb-flip" data-effect="y" data-height="equal">
              <div class="flipbox-front-layout cfb-data">
                <div class="flipbox-image-content">
                  <div class="flipbox-image-top"><img src="<?php echo $img; ?>stability-chamber-calibration-e1588169749377.jpg" alt="" /><div class="flip-icon-bototm flipbox-icon" style="font-size:200px;border-color:#b53426;color:#b53426">
                        <i class="fa fa-adjust"></i>
                       </div></div>
                  <div class="flipbox-img-content">
                    <h5 style="color:#b53426">Calibration Of Stability Chambers</h5>
                  </div>
                </div>
                </div>
              <div class="flipbox-back-layout cfb-data" style="background-color:#b53426">
                <p>Stability CHambers of all types calibrated over the range from -80°C Upto 1100°C and 0% to 100% RH using a highly accurate Rotronic Make Thermo Hygrometer.</p></div>
            </div>
          </div></div></p>
</div></div>
			</div>
			</div><!-- .entry-content -->

	</article>
		</div><!-- #primary -->
	</div>

</div><!-- #main -->
</section><!-- section -->

<?php include "main/footer.php";?>