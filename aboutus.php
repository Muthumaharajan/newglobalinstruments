<?php include "main/header.php"; ?>
	<div id="content" class="main-container">
					<div class="header-callout">
									</div>
		
		<section class="content-area  pt0 ">
			<div id="main" class="container" role="main">
	<div class="row">
				<div id="primary" class="col-md-8 mb-xs-24 no-sidebar">
																
<article id="post-7" class="post-7 page type-page status-publish hentry">
	<header class="entry-header">
				<h1 class="entry-title">About Us</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		
<h2 class="has-text-align-center">Welcome To New Global Instruments.</h2>



<div class="wp-block-cover aligncenter"><span aria-hidden="true" class="wp-block-cover__background has-background-dim"></span><img class="wp-block-cover__image-background wp-image-176" alt="" src="<?php echo $img; ?>nlogo/Picture9.jpg" data-object-fit="cover" srcset="<?php echo $img; ?>nlogo/Picture9.jpg 1920w, <?php echo $img; ?>nlogo/Picture9.jpg 600w, <?php echo $img; ?>nlogo/Picture9.jpg 300w, <?php echo $img; ?>nlogo/Picture9.jpg 1024w, <?php echo $img; ?>nlogo/Picture9.jpg 768w, <?php echo $img; ?>nlogo/Picture9.jpg 1536w" sizes="(max-width: 1920px) 100vw, 1920px" /><div class="wp-block-cover__inner-container">
<h3 class="has-text-align-center">Our Journey !!!</h3>
</div></div>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">New Global Instruments has come a long way with lot of new services as well as products added to its list. New Global Instruments Laboratory in Qatar strives to provide the best calibration, temperature mapping of storage areas as well as validation solutions to their clients and to become their preferable calibration and temperature mapping service provider in Qatar
</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">As maintenance, calibration, thermal mapping, validation and qualification constitute fundamental operations in any industry, New Global Instruments Laboratory is dedicated to assist its Clients in developing, implementing and maintaining the quality and traceability of the accuracy of their laboratory and instruments in order to ensure the full compliance with all international guidelines as  per  ISO 17025:2017 and ISO 9001: 2015
</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color"><strong>Vision</strong></span></p>



<p class="has-text-align-justify"><strong><span style="color:#000000" class="has-inline-color"><br /></span></strong><em><span style="color:#000000" class="has-inline-color">We strive to change the future of calibration, temperature mapping, validation and maintenance in India by providing the clients with the most reliable, efficient and economical services that exhibit the spirit of innovation & excellence.</span></em></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color"><strong>Mission</strong></span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color"><br /><em>To become the most reliable calibration service partner of the clients. Besides, we ensure their instruments are calibrated and validated precisely &amp; therefore we work sincerely to meet their high quality and accuracy requirements. After each calibration and validation, our engineers conduct a check to ensure the instruments attain their benchmarks and precise values at all times.</em></span></p>



<figure class="wp-block-image aligncenter size-large is-resized is-style-rounded"><img src="<?php echo $img; ?>desk-setup-2.jpg" alt="" class="wp-image-198" width="588" height="367" srcset="<?php echo $img; ?>desk-setup-2.jpg 1000w, <?php echo $img; ?>desk-setup-2-600x375.jpg 600w, <?php echo $img; ?>desk-setup-2-300x188.jpg 300w, <?php echo $img; ?>desk-setup-2-768x480.jpg 768w" sizes="(max-width: 588px) 100vw, 588px" /></figure>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color"><strong>Our Principles</strong></span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color"><strong><br /></strong>Building partnerships with our customers by providing world-class services and products.<br />Develop processes that produce services in compliance with customer and market expectations, needs and demands.<br />Treating our employees with respect, offering them opportunities to contribute and develop their full potential while we share with them the rewards and responsibilities of their achievements.<br />Being responsible corporate citizens in our communities by maintaining high ethical business &amp; environmental standards.</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">All performed services are documented with comprehensive calibration certificates, raw data sheets and master traceable reports that include all comments, protocols, reference standard instrument issued.</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">New Global Instruments Laboratory proposes as well to its Customers, technical support by implementing periodic maintenance programs following client request and production schedule.</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color"><strong>Why Us?</strong></span></p>



<p class="has-text-align-justify"><strong><span style="color:#000000" class="has-inline-color"><br /></span></strong><span style="color:#000000" class="has-inline-color">We have become a one-stop destination for the most reliable calibration, temperature mapping of warehouse and validation solutions provider in Qatar, Dubai, and GCC Singapore Malaysia and India   </span> <span style="color:#000000" class="has-inline-color">across the country owing to the following reasons :</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">100% reliability</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">Competent team</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">Vast industry experience</span></p>



<p class="has-text-align-justify"><mark style="background-color:rgba(0, 0, 0, 0);color:#000000" class="has-inline-color">Client-centric approach</mark></p>



<p class="has-text-align-justify">Digital Signed Easily Accessible Reports</p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">Nominal rates</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">Quick execution</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">Ethical business practices</span></p>



<p class="has-text-align-justify"><span style="color:#000000" class="has-inline-color">Transparent dealings</span></p>
	</div><!-- .entry-content -->
		<footer class="entry-footer">
			</footer><!-- .entry-footer -->
</article><!-- #post-## -->
		</div><!-- #primary -->
			</div>

</div><!-- #main -->
</section><!-- section -->
<?php include "main/footer.php"; ?>