<?php include "main/header.php";?>

<div id="content" class="main-container">
					<div class="header-callout">
									</div>
		
		<section class="content-area  pt0 ">
			<div id="main" class="container" role="main">
	<div class="row">
		<div id="primary" class="col-md-8 mb-xs-24 sidebar-right">
			<article id="post-2" class="post-content post-grid-wide post-2 page type-page status-publish has-post-thumbnail hentry">
	<header class="entry-header nolist">
				<a href="index.html">
			<img width="600" height="300" src="<?php echo $img; ?>temperature-mapping-study-vackerglobal.png" class="attachment-shapely-featured size-shapely-featured wp-post-image" alt="" srcset="<?php echo $img; ?>temperature-mapping-study-vackerglobal.png 600w, <?php echo $img; ?>temperature-mapping-study-vackerglobal-300x150.png 300w" sizes="(max-width: 600px) 100vw, 600px" />		</a>

					</header><!-- .entry-header -->
	<div class="entry-content">
					<h2 class="post-title entry-title">
				<a href="index.html">Temperature Mapping Services</a>
			</h2>
		
		<div class="entry-meta">
			
		<ul class="post-meta">
							<li><span>by <a href="index.php" title="New Global Instruments.">New Global Instruments.</a></span></li>
				</ul>
					<!-- post-meta -->
		</div>

					<div class="shapely-content dropcaps-content">
				
<div class="wp-block-group"><div class="wp-block-group__inner-container">
<p class="has-text-align-justify">New Global Instruments based in Qatar, has been providing temperature mapping of storage area services to its clients. Also having its branches in Dubai and India .</p>



<p class="has-text-align-justify">Dedicated to providing you with long-term value, our team of temperature humidity mapping and validation experts have the expertise to ensure you are audit ready and that your facility and equipment meet FDA validation requirements. We do this through our comprehensive knowledge of regulated industries.</p>



<p class="has-text-align-justify"><em><strong>So let us understand What is Temperature Mapping? Why do Temperature Mapping of Warehouse or Cold Storages?</strong></em></p>



<p class="has-text-align-justify">Temperature mapping of storage areas is a mandatory requirement whenever regulatory controlled product is being manufactured, and thermal processing or storage is involved (heating or cooling).&nbsp; The individual Operational Qualification (OQ) of the equipment in use, must call for heat distribution; temperature mapping protocol, studies of the actual heating or cooling process area used (Cabinet, Room or container). This Temperature Mapping As Per WHO study must verify that all product will be subject to the same thermal environmental conditions; no matter where the product is situated within the processing compartment.&nbsp; This verification is mandatory and applies to all environmental process and storage equipment such as freezers, refrigerators, cold rooms, cool rooms, warehouses, delivery vans or Reefer vehicles, transport containers, stability chambers, freeze dryers and incubators.</p>



<p class="has-text-align-justify">During and after manufacture, the product is often held in storage areas or warehouses.&nbsp; The environmental conditions in these facilities are traditionally held to human comfort levels and that is purely within the physical level; humans are working within.&nbsp; Large fabricated metal storage warehouses can show enormous variations in both heat gains and heat losses.&nbsp; They can in certain locations and certain seasonal times hit highs of plus 60 degrees centigrade and lows of minus 30 degrees centigrade.&nbsp; This renders it essential to ensure your validation performance qualification (PQ) verifies that during these extremes of temperature your product is maintained within the temperature range specified by the product makers.&nbsp;&nbsp;</p>



<p class="has-text-align-justify">Heat distribution studies of large storages and warehouses requires sensors to be fitted strategically throughout the storage shelving, covering a percentage of the shelving area horizontally and vertically.&nbsp;</p>



<p class="has-text-align-justify"><em><strong>So now, let us understand how does New global instruments perform the temperature mapping for warehouse for its clients?&nbsp;</strong></em></p>



<p class="has-text-align-justify">Temperature Mapping As Per WHO is started with primarily inspection of the area where temperature mapping of storage area is to be performed and deciding the number of temperature mapping data loggers and temperature mapping sensor location of the temperature mapping loggers. Once this is finalized, our New global instruments engineers start with the mounting of the wireless temperature mapping warehouse data loggers as per defined positions. Once the temperature mapping data loggers are mounted, we start with the Operational Qualification Study. This continuous cycle test study is to be carried out for 24 hours or 3 days or 7 days depending upon the client protocol. Operational Qualification Study Cycle is also know as Empty Cycle because the temperature mapping of storage area study is performed in empty storage area.&nbsp;</p>



<p class="has-text-align-justify">After successful completion of OQ Cycle, our New global instruments Engineers proceed with the Performance Qualification Study which is also known as Load Cycle Test because the temperature mapping study is performed in loaded storage area. During PQ study, the position of wireless temperture mapping data loggers is kept same as in OQ.&nbsp;</p>



<p class="has-text-align-justify">This continuous cycle test study is to be carried out again for 24 hours or 3 days or 7 days depending upon the client temperature mapping protocol and temperature mapping guidelines.&nbsp;</p>



<p class="has-text-align-justify">After the continuous cycle test is performed for Operational Qualification and Performance Qualification, recovery tests such as Door Open Test and Power Failure Test is performed.&nbsp;</p>



<p class="has-text-align-justify">Also, on the basis of the OQ and PQ Cycle test, Temperature Mapping Hot Spot and Cold Spots are identified based on the data extracted from each and every temperature mapping data logger and these spots are pointed out in the temperature mapping diagram for temperature mapping storage area so clients can keep their critical products accordingly.&nbsp;</p>



<p><em><strong>What is Temperature Mapping MKT?&nbsp;</strong></em></p>



<p class="has-text-align-justify">The FDA regards Temperature Mapping MKT (Mean Kinetic Temperature) as a calculation that will show if a product has exceeded storage conditions. Temperature Mapping MKT can also be used to determine if storage, handling, shipment, etc. have affected the shelf life of the product. The Temperature Mapping MKT is a calculated fixed temperature that simulates the effects of temperature variations on the product&nbsp; over a period of time. It expresses the cumulative thermal stress experienced by a product at varying temperatures during storage. In addition to Temperature Mapping MKT, our warehouse temperature mapping reports also include minimum and maximum values of every logger including a description of the place and time of occurrence.</p>



<p><em><strong>Now let us understand what is Door Open Test?</strong></em></p>



<p class="has-text-align-justify">The refrigerator or freezer or Cold Room door may be kept open while taking the medicines out or keeping it inside. Once the door is opened, the atmospheric hot air may start flowing inside. Door opening test is conducted to determine how long the door can be kept opened without exceeding the limited temperature range.</p>



<p><em><strong>What is Power Failure Test?</strong></em></p>



<p class="has-text-align-justify">Power failure test is also known as hold over time test. To determine how long the unit can hold the specified temperature range after the unit power is off without exceeding the limited temperature range.</p>



<p class="has-text-align-justify">The frequency of the temperature mapping is a minimum of at least once a year. Additional temperature mapping may be required for changes such as Change of temperature settings. Changes made to the storage capacity. New monitoring system. New equipment installed that may impact the temperature and relative humidity.&nbsp;</p>



<p class="has-text-align-justify">If the continuous temperature mapping activities are interrupted due to power failures or any event that may impair the continuous data collection.Extreme weather changes.Warehousing standards or quality management system changes. In areas with potentially high temperature fluctuations additional temperature recording devices must be strategically located Near windows, ceilings and exits, Skylights, air-blowers, ducts and A/C units, Areas with high traffic movement and activities or Near heat sources.&nbsp;</p>



<p class="has-text-align-justify">Be rest assured once New global instruments is on field for your Temperature Mapping Service requirement.</p>



<p><strong>New Global Instruments. Experience You Can Trust&#8230;</strong></p>



<p></p>
</div></div>



<div class="wp-block-jetpack-slideshow aligncenter" data-autoplay="true" data-delay="3" data-effect="fade"><div class="wp-block-jetpack-slideshow_container swiper-container"><ul class="wp-block-jetpack-slideshow_swiper-wrapper swiper-wrapper"><li class="wp-block-jetpack-slideshow_slide swiper-slide"><figure><img alt="" class="wp-block-jetpack-slideshow_image wp-image-337" data-id="337" src="<?php echo $img; ?>warehouse-1.jpg" srcset="<?php echo $img; ?>warehouse-1.jpg 724w, <?php echo $img; ?>warehouse-1-600x370.jpg 600w, <?php echo $img; ?>warehouse-1-300x185.jpg 300w" sizes="(max-width: 724px) 100vw, 724px" /></figure></li><li class="wp-block-jetpack-slideshow_slide swiper-slide"><figure><img alt="" class="wp-block-jetpack-slideshow_image wp-image-336" data-id="336" src="<?php echo $img; ?>sensor_distribution-1.jpg" srcset="<?php echo $img; ?>sensor_distribution-1.jpg 1051w, <?php echo $img; ?>sensor_distribution-1-600x481.jpg 600w, <?php echo $img; ?>sensor_distribution-1-300x240.jpg 300w, <?php echo $img; ?>sensor_distribution-1-1024x820.jpg 1024w, <?php echo $img; ?>sensor_distribution-1-768x615.jpg 768w" sizes="(max-width: 1051px) 100vw, 1051px" /></figure></li><li class="wp-block-jetpack-slideshow_slide swiper-slide"><figure><img alt="" class="wp-block-jetpack-slideshow_image wp-image-335" data-id="335" src="../wp-content/uploads/2020/04/EJ5g-caXYAEC_pX-1.png" srcset="https://newglobalinstruments.com/wp-content/uploads/2020/04/EJ5g-caXYAEC_pX-1.png 567w, <?php echo $img; ?>EJ5g-caXYAEC_pX-1-300x204.png 300w, <?php echo $img; ?>EJ5g-caXYAEC_pX-1-220x150.png 220w" sizes="(max-width: 567px) 100vw, 567px" /></figure></li><li class="wp-block-jetpack-slideshow_slide swiper-slide"><figure><img alt="" class="wp-block-jetpack-slideshow_image wp-image-334" data-id="334" src="<?php echo $img; ?>Ten-X_YearEndOutlook_2017.jpg" srcset="<?php echo $img; ?>Ten-X_YearEndOutlook_2017.jpg 768w, <?php echo $img; ?>Ten-X_YearEndOutlook_2017-600x415.jpg 600w, <?php echo $img; ?>Ten-X_YearEndOutlook_2017-300x207.jpg 300w" sizes="(max-width: 768px) 100vw, 768px" /></figure></li><li class="wp-block-jetpack-slideshow_slide swiper-slide"><figure><img alt="" class="wp-block-jetpack-slideshow_image wp-image-333" data-id="333" src="../wp-content/uploads/2020/04/temperature-mapping-study-vackerglobal.png" srcset="<?php echo $img; ?>temperature-mapping-study-vackerglobal.png 600w, <?php echo $img; ?>temperature-mapping-study-vackerglobal-300x150.png 300w" sizes="(max-width: 600px) 100vw, 600px" /></figure></li><li class="wp-block-jetpack-slideshow_slide swiper-slide"><figure><img alt="" class="wp-block-jetpack-slideshow_image wp-image-332" data-id="332" src="<?php echo $img; ?>maxresdefault-2.jpg" srcset="<?php echo $img; ?>maxresdefault-2.jpg 1280w, <?php echo $img; ?>maxresdefault-2-600x338.jpg 600w, <?php echo $img; ?>maxresdefault-2-300x169.jpg 300w, <?php echo $img; ?>maxresdefault-2-1024x576.jpg 1024w, <?php echo $img; ?>maxresdefault-2-768x432.jpg 768w" sizes="(max-width: 1280px) 100vw, 1280px" /></figure></li></ul><a class="wp-block-jetpack-slideshow_button-prev swiper-button-prev swiper-button-white" role="button"></a><a class="wp-block-jetpack-slideshow_button-next swiper-button-next swiper-button-white" role="button"></a><a aria-label="Pause Slideshow" class="wp-block-jetpack-slideshow_button-pause" role="button"></a><div class="wp-block-jetpack-slideshow_pagination swiper-pagination swiper-pagination-white"></div></div></div>
			</div>
			</div><!-- .entry-content -->

	</article>
		</div><!-- #primary -->
		

<aside id="secondary" class="widget-area col-md-4" role="complementary">
	<div id="media_image-3" class="widget widget_media_image"><h2 class="widget-title">Probe Placement</h2><img width="300" height="204" src="../wp-content/uploads/2020/04/EJ5g-caXYAEC_pX-1-300x204.png" class="image wp-image-335  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="<?php echo $img; ?>EJ5g-caXYAEC_pX-1-300x204.png 300w, <?php echo $img; ?>EJ5g-caXYAEC_pX-1-220x150.png 220w, <?php echo $img; ?>EJ5g-caXYAEC_pX-1.png 567w" sizes="(max-width: 300px) 100vw, 300px" /></div><div id="media_image-5" class="widget widget_media_image"><h2 class="widget-title">Cold Room Temp RH Mapping</h2><img width="300" height="240" src="<?php echo $img; ?>sensor_distribution-1-300x240.jpg" class="image wp-image-336  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="<?php echo $img; ?>sensor_distribution-1-300x240.jpg 300w, <?php echo $img; ?>sensor_distribution-1-600x481.jpg 600w, <?php echo $img; ?>sensor_distribution-1-1024x820.jpg 1024w, <?php echo $img; ?>sensor_distribution-1-768x615.jpg 768w, <?php echo $img; ?>sensor_distribution-1.jpg 1051w" sizes="(max-width: 300px) 100vw, 300px" /></div><div id="media_image-6" class="widget widget_media_image"><h2 class="widget-title">DataLogger Placement in Warehouse</h2><img width="300" height="185" src="../wp-content/uploads/2020/04/warehouse-1-300x185.jpg" class="image wp-image-337  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="<?php echo $img; ?>warehouse-1-300x185.jpg 300w, <?php echo $img; ?>warehouse-1-600x370.jpg 600w, <?php echo $img; ?>warehouse-1.jpg 724w" sizes="(max-width: 300px) 100vw, 300px" /></div><div id="media_image-4" class="widget widget_media_image"><h2 class="widget-title">Hot Spot &#038; Cold Spot</h2><img width="300" height="150" src="<?php echo $img; ?>temperature-mapping-study-vackerglobal-300x150.png" class="image wp-image-333  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="<?php echo $img; ?>temperature-mapping-study-vackerglobal-300x150.png 300w, <?php echo $img; ?>temperature-mapping-study-vackerglobal.png 600w" sizes="(max-width: 300px) 100vw, 300px" /></div><div id="media_image-7" class="widget widget_media_image"><h2 class="widget-title">How To Carry Out Temperature Mapping</h2><img width="300" height="169" src="<?php echo $img; ?>maxresdefault-2-300x169.jpg" class="image wp-image-332  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="<?php echo $img; ?>maxresdefault-2-300x169.jpg 300w, <?php echo $img; ?>maxresdefault-2-600x338.jpg 600w, <?php echo $img; ?>maxresdefault-2-1024x576.jpg 1024w, <?php echo $img; ?>maxresdefault-2-768x432.jpg 768w, <?php echo $img; ?>maxresdefault-2.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px" /></div><div id="media_image-8" class="widget widget_media_image"><h2 class="widget-title">Warehouse Mapping</h2><img width="300" height="218" src="../wp-content/uploads/2020/04/WAREHOUSE-MONITORING-300x218.png" class="image wp-image-346  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;" srcset="https://newglobalinstruments.com/wp-content/uploads/2020/04/WAREHOUSE-MONITORING-300x218.png 300w, https://newglobalinstruments.com/wp-content/uploads/2020/04/WAREHOUSE-MONITORING.png 550w" sizes="(max-width: 300px) 100vw, 300px" /></div><div id="media_image-9" class="widget widget_media_image"><h2 class="widget-title">Cold Room Temperature Mapping</h2><img width="536" height="600" src="<?php echo $img; ?>screenshot-www.who_.int-2019-01-30-09-19-15-843.jpg" class="image wp-image-347  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" srcset="<?php echo $img; ?>screenshot-www.who_.int-2019-01-30-09-19-15-843.jpg 536w, <?php echo $img; ?>screenshot-www.who_.int-2019-01-30-09-19-15-843-268x300.jpg 268w" sizes="(max-width: 536px) 100vw, 536px" /></div><div id="media_image-10" class="widget widget_media_image"><h2 class="widget-title">Reefer Vehicle Temperature Mapping</h2><img width="480" height="317" src="../wp-content/uploads/2020/04/i-1.webp" class="image wp-image-348  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" /></div></aside><!-- #secondary -->
	</div>

</div><!-- #main -->
</section><!-- section -->

<?php include "main/footer.php";?>